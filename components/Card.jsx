export default function Card() {
  return (
    <div className="m-auto w-[350px] bg-white p-7 rounded-lg drop-shadow">
      <div className="flex flex-row gap-2">
        <div className="w-[70px] h-[70px] bg-[#8d69bf] rounded-md"></div>
        <div className="flex flex-col">
          <p>Name</p>
          <p>Passport number</p>
          <p>Policy ID number</p>
        </div>
      </div>
      <div className="flex items-center justify-center w-[300px] mt-6 border border-[#e4e7eb]"></div>
      <div className="flex flex-col space-y-2">
        <h1 className="font-semibold mt-3">Trip details</h1>
        <div className="flex flex-row justify-between">
          <span className="text-[#6a7280]">Country of Origin</span>
          <span className="font-bold text-[#8d69bf]">Ghana</span>
        </div>
        <div className="flex flex-row justify-between">
          <span className="text-[#6a7280]">Coverage starts</span>
          <span className="font-bold text-[#8d69bf]">Aug 12,2023</span>
        </div>{" "}
        <div className="flex flex-row justify-between">
          <span className="text-[#6a7280]">Coverage Ends</span>
          <span className="font-bold text-[#8d69bf]">Sep 11,2023</span>
        </div>{" "}
        <div className="flex flex-row justify-between">
          <span className="text-[#6a7280]">Country of Origin</span>
          <span className="font-bold text-[#8d69bf]">Ghana</span>
        </div>{" "}
        <div className="flex flex-row justify-between">
          <span className="text-[#6a7280]">Duration</span>
          <span className="font-bold text-[#8d69bf]">30 days</span>
        </div>
      </div>
      <div className="relative flex items-center justify-center w-[300px] mt-6 border border-t border-[#e4e7eb]"></div>
      <div>
        <h1 className="font-semibold mt-3">Standard Plan</h1>
        <div className="flex flex-row justify-between mt-3 items-center">
          <span className="text-[#6a7280]">Price</span>
          <span className="font-bold text-[#8d69bf] text-2xl">$45.00</span>
        </div>
      </div>
    </div>
  );
}
